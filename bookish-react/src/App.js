import React, {Component} from 'react';
import Books from './components/Books/Books'
import axios from 'axios';
import './App.css';

class App extends Component {

  state = {
    books: []
  }

  componentDidMount(){
    axios.get('http://localhost:8080/books').then(data => {
     this.setState({
        books: data.data
      })
    })
  }

  render(){
    return (
      <div className="App">
        <h1>Pokemon</h1>
        <Books books={this.state.books} />
      </div>
    );
  }
}

export default App;
