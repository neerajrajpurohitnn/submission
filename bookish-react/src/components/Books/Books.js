import React from 'react';

const Books = props => (
    <div className = "Books">
        {
            props.books.map(book => {
                return (
                    <div className="book" key = { Math.random() }>
                        <h2 className="book-title">{book.name}</h2>
                    </div>
                )
            })
        }
    </div>  
);

export default Books;
