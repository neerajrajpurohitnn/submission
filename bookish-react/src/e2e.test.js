import puppeteer from 'puppeteer';
const appUrlBase = 'http://localhost:3000';
import axios from 'axios';
let browser, page;
beforeAll(async () => {
  browser = await puppeteer.launch({})
  page = await browser.newPage()
})

describe('Book Library', () => {
  test('Content', async () => {
    await page.goto(`${appUrlBase}/`)
    await page.waitForSelector('h1')
    const result = await page.evaluate(() => {
      return document.querySelector('h1').innerText
    })
    // console.log(expect(result).toEqual('Pokemon'))
    expect(result).toEqual('Pokemon')

  })

  test('Book List', async () => {
        await page.goto(`${appUrlBase}/`)
        await page.waitForSelector('.Books')
        const books = await page.evaluate(() => {
          return [...document.querySelectorAll('.book .book-title')].map(el => el.innerText)
        })
    
        expect(books.length).toEqual(2)
        expect(books[0]).toEqual('Refactoring')
       expect(books[1]).toEqual('Domain-driven design')
      })
})


afterAll(() => {
  browser.close()
})



